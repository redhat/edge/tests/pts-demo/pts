#!/usr/bin/env python3

# Test for ansible

from datetime import datetime
import os
from pprint import pprint
import re
import json
import hashlib
import argparse


def create_es_obj(user, clustername, uuid):
	return {
		"_index": "system_attributes",
		"_op_type": "create",
		"_source": {
			"user": user,
			"clustername": clustername,
			"uuid": uuid,
			"date": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
		},
		"_id": "",
	}


def get_ansible_facts(hostname):
	stream = os.popen(f'ansible -m ansible.builtin.setup -i {hostname}, all')

	dev_facts = stream.read()
	output = re.sub(r"\A.*=>", "", dev_facts, re.DOTALL)

	json_output = json.loads(output)
	return json_output['ansible_facts']


def parse_os_release():
	#output = (stream.readline()).split(':')
	#output = [x.strip() for x in output]
	return


def select_ansible_fields(ansible_facts, selected_fields):
	system_config = {}
	for field in selected_fields:
		system_config[field] = ansible_facts[f'ansible_{field}']
	return system_config


def add_json_to_archive(filename, name, obj):
	'''
	Adds the obj into every line of benchmark-wrapper's archive file
	'''
	# Open up existing archive file and put in system config
	with open(filename, "r+") as lines:
		output_archive = ""
		for line in lines:
			test_run = json.loads(line)
			test_run['_source']['system_config'] = obj
			json_output = json.dumps(test_run)
			output_archive += re.sub(r"\n", "", json_output, re.DOTALL) + '\n'
		lines.seek(0)
		lines.write(output_archive)
		lines.truncate()

	return output_archive

def process_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("host", help="name of the host")
    parser.add_argument("-f", "--filename", help="name of the file")
    parser.add_argument("-a", "--all", help="all dev facts", action="store_true")

    # Parse the arguments
    args = parser.parse_args()

    # Return a dictionary containing the arguments
    return (
        args.host,
        args.filename,
        args.all,
    )

def main(argv):


	(hostname, filename, all_facts) = process_args()

	# Get ansible data and put it into th Elasticsearch object
	ansible_facts = get_ansible_facts(hostname)

	fields = ['nodename',
           'distribution',
           'distribution_major_version',
           'kernel',
           'distribution_version',
           'machine',
           'processor_count',
	   'kernel_version',
           #'processor',
           'product_name',
           'memory_mb',
           ]

	system_config = ansible_facts if all_facts else select_ansible_fields(ansible_facts, fields)

	add_json_to_archive(filename, 'system_config', system_config) if filename else print(system_config)

	#(test_user, clustername) = (os.environ['test_user'], os.environ['clustername'])
	#es = create_es_obj(test_user, clustername, uuid)
	#es['_source'].update(system_config)
	#es['_id'] = hashlib.sha256(str(es).encode()).hexdigest()
	#es_json = json.dumps(es)
	#es_json = re.sub(r"\n","", es_json, re.DOTALL)
	#print(es_json)
if __name__ == '__main__':
	import sys
	main(sys.argv)
