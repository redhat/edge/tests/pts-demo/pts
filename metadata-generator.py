#!/usr/bin/env python 

import os
import pwd
import sys
import uuid
import re
import hashlib
from datetime import datetime
import configargparse
import json
import dateutil.parser
import socket
from dateutil import tz


def parse_args():

    parser = configargparse.ArgumentParser()
    parser.add_argument(
            '-n',
            '--cluster-name', help='Descriptive name of the system. Defaults to hostname of current system',
            required=False
            )
    parser.add_argument(
            '-u',
            '--user',
            help='Name of the intitator of the test, Defaults to current user on system.',
            required=False
            )
    parser.add_argument(
            '-i',
            '--uuid',
            help='Unique identifier to associate with your test. Defaults to a randomly generated one.',
            required=False
            )
    parser.add_argument(
            '-e',
            '--es-index',
            help='Name of the Elasticserach index to store the data',
            required=True
            )
    parser.add_argument(
            '-d',
            '--date',
            help='Date time stamp to associate with your test. If not specified, will try to look \
                  in the --json-file, otherwise will populate with current time',
            required=False
            )
    parser.add_argument(
            '-r',
            '--run-id',
            help='??. Defaults to `NA`',
            required=False
            )
    parser.add_argument(
            '-s',
            '--sample',
            help='The index of the current sample. Defaults to 0.',
            required=False,
            )
    parser.add_argument(
            '-c',
            '--config',
            help='Config file',
            is_config_file=True,
            required=False
            )
    parser.add_argument(
            '-j',
            '--json-file',
            help='JSON file to be converted to a benchmark-wrapper archive file',
            required=False
            )
    return parser.parse_args()

def process_args(args):
    if args.uuid is None:
        args.uuid = str(uuid.uuid4())

    if args.user is None:
        args.user = pwd.getpwuid(os.getuid()).pw_name

    if args.cluster_name is None:
        args.cluster_name = socket.gethostname()

    if args.sample is None:
        args.sample = 0

    if args.run_id is None:
        args.run_id = "NA"

    if args.date is None:
        args.date = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")

# Parse timestamp to be compatitible with benchmark wrapper
def standardize_timestamp(timestamp):
    utc_tz = tz.gettz("UTC")
    time_obj = dateutil.parser.parse(timestamp)
    return time_obj.astimezone(utc_tz).strftime("%Y-%m-%dT%H:%M:%S.%fZ")

def convert_json_to_bw_archive(json):
    return re.sub(r"\n", "", json, re.DOTALL)


def generate_id(source):
    return hashlib.sha256(str(source).encode()).hexdigest()


def generate_template(args):
    return {
            "_index": args.es_index,
            "_op_type": "create",
            "_source": {
                "test_config": {},
                "test_results": {},
                "date": args.date,
                "cluster_name": args.cluster_name,
                "user": args.user,
                "sample": args.sample,
                "run_id": args.run_id,
                "uuid": args.uuid,
                },
            "_id": "",
            }


def main():
    args = parse_args()
    process_args(args)

    if args.json_file:
        with open(args.json_file, 'r', encoding='utf-8') as file:
            json_data = json.load(file)

        # If JSON does not contain a array, make it into a list to conform with the processing
        if not isinstance(json_data, list):
            json_data = [json_data]
    else:
        # No JSON file specified, just generate a empty template
        json_data = [ { "test_config": { "source": "metdata-generator" } } ]


    submission_date = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    output_filename = f'{args.user}_{submission_date}.archive'
    for submission in json_data:

        # Create the output dcitoinary
        bw_output = generate_template(args)
        bw_output["_source"].update(submission)
        bw_output["_source"]["date"] = standardize_timestamp(bw_output["_source"]["date"])
        bw_output["_id"] = generate_id(bw_output["_source"])

        with open(output_filename, 'a') as f:
            json_output = json.dumps(bw_output)
            output_archive = convert_json_to_bw_archive(json_output)
            f.write(output_archive)
        print(output_archive)

if __name__ == "__main__":
    sys.exit(main())
