Performance Test Suite (PTS)

To run the Python scripts it is recommended to use a virtual env
```bash
pip3 install venv
python3 -m venv pts-env
source pts-env/bin/activate
pip3 install -r requirements.txt
```

## [metadata-generator](metadata-generator.py)

Script to generate the necessary metadata for a [benchmark-wrapper](https://github.com/cloud-bulldozer/benchmark-wrapper) archive file that can be pushed to Elasticsearch.

Use cases:
- Prototyping or one off test results to push to Elasticsearch
- Pushing a collection of results to Elasticsearch

Outputs a `.archive` file with the current username with a timestamp.


### Arguments

Arguments can be specified in one of three ways in the following high to low priority:
1. JSON file with the results with the exception of `--es-index`
2. command line
3. config file based from [ConfigArgParse](https://github.com/bw2/ConfigArgParse#config-file-syntax)

Full list of arguments can be found using `--help`

### JSON file format

Script can accept a single JSON entry or an array if multiple entries.

Recommended format to have to keep consistency:

```json
// Array is optional if only a single record
[
    {
        // Override command line and config file
        "date": "2022-01-14T15:11:40.128470Z",
        "cluster_name": "system_under_test",
        "user": "john_doe",
        "sample": 0,
        "run_id": "NA",
        "uuid": "c7357b0-c852-4626-abec-29dd570996f5",

        # Actual recommended structure for test
        "test_config": {
            // Parameters passed into to the test
        },
        "test_results": {
            // Test results here
        },
        "system_config": {
            // Optional system configuration data
        }
    }
]
```

### Examples

#### Barebones JSON file

Example where the bare minimal fields are provided to fufill the need of developing a benchmark.

```bash
./metadata-generator.py --es-index my-elasticsearch-index -j results.json
```

##### results.json:

```json
{
    "test_results": {
        "result": "success"
    }
}
```

#### JSON file with multiple results and all fields filled out

Command to push the contents of `results.json` to Elasticsearch. 

```bash
./metadata-generator.py --es-index my-elasticsearch-index -j results.json
```

##### results.json:

```json
[
    {
        "date": "2022-01-14T15:11:40.128470Z",
        "cluster_name": "system_under_test",
        "test_results": {
            "result": "success"
        }
    },
    {
        "date": "2022-01-14T15:11:40.128470Z",
        "cluster_name": "another_system_under_test",
        "test_results": {
            "result": "failed"
        }
    }
]
```

## [trigger-pts](trigger-pts.py)

Script to submit test results and PCP metrics to Elasticsearch using PTS' 
existing GitLab CI pipelines. The script takes the specified files and uploads 
it to an __S3 bucket with public access__ and then triggers the PTS pipeline
with the URL as the payload for it to download. 

Some use cases:
- Take the output from [metadata-generator](metadata-generator.py)
and push it to Elasticsearch without having to install any dependencies
- Pushing PCP metrics that have been converted with 
[pcp2json](https://man7.org/linux/man-pages/man1/pcp2json.1.html) and associate
with an archive file.

### Setup

#### GitLab Access Token

Requires the ability to run Pipelines in 
[PTS](https://gitlab.com/redhat/edge/tests/pts-demo/pts/-/pipelines).

1. [Create GitLab Token](https://gitlab.com/-/profile/personal_access_tokens?name=Trigger+PTS+Access+token&scopes=api)
2. Token must be specified as an environment variable `GITLAB_TOKEN`.

#### S3 bucket

The script requires a S3 bucket to upload files to. The script can upload to S3
buckets anonymously by using the `--no-credentials` flag.

For credentials it supports the following:
    - Environment variables:
        - `AWS_ACCESS_KEY_ID`
        - `AWS_SECRET_ACCESS_KEY`
    - Credentails configured by AWS-CLI 
    - Others as listed by [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html#configuring-credentials).

The URL to the s3 bucket can be stored as an environment variable 
`PTS_S3_BUCKET` or specified at CLI with `--bucket-name`.

## Arugments

Full list of arguments can be found using `--help`.

## Examples

Assumes the following environment variables are set:

### Submitting an archive file only

```bash
./trigger-pts.py --s3-url https://s3.us-east-1.amazonaws.com \
  --bucket-name my-s3-bucket \
  --archive-file my-archive-file.archive 
```
### Submitting an archive file to an annoymous S3 bucket

Example of submiting an archive S3 bucket that does not require credentials.

```bash
./trigger-pts.py  --s3-url https://s3.us-east-1.amazonaws.com \
  --no-credentials \
  --bucket-name my-s3-bucket \
  --token my_gitlab_token \
  --archive-file my-archive-file.archive \
  --json-file my-pcp.json
```

### Submitting PCP data

```bash
./trigger-pts.py  --s3-url https://s3.us-east-1.amazonaws.com \
  --bucket-name my-s3-bucket \
  --token my_gitlab_token \
  --archive-file my-archive-file.archive \
  --json-file my-pcp.json
```

