usage() {
	echo "$0 <full path to archive file>" 1>&2
	echo "Pushes an benchmark-wrapper archive file to Elasticsearch"
	exit 1;
}


archive_file=$1


[ -z "$es" ] && echo -e "Error: The 'es' enviornment variable noet set. Specify a Elasticsearch server. Example:\n\n\t export es=http://es-server:9200\n\n" && exit 1
if [ "$#" -lt 1 ]; then
  usage
fi

# Determine what architecture the script is being run on so that it can pick the right benchmark-wrapper container
arch=$(uname -m)
if [ $arch = "x86_64" ]
then
  arch="amd64"
fi

[ $(uname) != "Darwin" ] && VOLUME_MOUNT_FLAG=":z"

# Uses coremark-pro container since there is no dedicated benchmark-wrapper container
CMD="podman run --rm -e es=$es -it -v ${archive_file}:/output/$(basename ${archive_file})$VOLUME_MOUNT_FLAG quay.io/cloud-bulldozer/sysbench:latest  run_snafu --tool archive --archive-file /output/$(basename $archive_file)"
echo $CMD
exec $CMD

